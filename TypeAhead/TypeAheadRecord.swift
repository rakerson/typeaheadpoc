//
//  TypeAheadRecord.swift
//  TypeAhead
//
//  Created by Robert Akerson on 1/29/16.
//  Copyright © 2016 com.Duffy. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

public class TypeAheadRecord: Object {
    public dynamic var ZipCode: String = ""
    public dynamic var City: String = ""
    public dynamic var State: String = ""
    public dynamic var lat: Double = 0.0
    public dynamic var lng: Double = 0.0
    
    
    override public static func primaryKey() -> String? {
        return "ZipCode"
    }
}
