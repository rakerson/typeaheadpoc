//
//  ViewController.swift
//  TypeAhead
//
//  Created by Robert Akerson on 1/29/16.
//  Copyright © 2016 com.Duffy. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift
import GeoQueries
import MapKit


class ViewController: UIViewController {
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    var centerPoint:TypeAheadRecord = TypeAheadRecord()
    var results: [TypeAheadRecord]?
    @IBOutlet var searchResults: UITextView!
    @IBOutlet var textOutput: UITextView!
    @IBOutlet var searchTB: UITextField!
    @IBOutlet var resultsCount: UITextField!
    override func viewDidLoad() {
        loadingIndicator.hidden = true
        super.viewDidLoad()
        let realm = try! Realm()
        let zipCheck = realm.objects(TypeAheadRecord)
        if(zipCheck.count == 0)
        {
        loadingIndicator.hidden = false
        loadingIndicator.startAnimating()
        loadZips()
        }
    }
    func loadZips(){
        let url = "http://www.robbakerson.com/typeahead/zipcodes.txt"
        Alamofire.request(.GET, url).validate().responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    let realm = try! Realm()
                    realm.beginWrite()
                    for zip in json.arrayValue {
                        let tRecord = TypeAheadRecord()
                        tRecord.City = zip["City"].stringValue
                        tRecord.ZipCode = zip["Zipcode"].stringValue
                        tRecord.State = zip["State"].stringValue
                        tRecord.lat = zip["Lat"].double!
                        tRecord.lng = zip["Long"].double!
                        realm.add(tRecord, update: true)
                    }
                    try! realm.commitWrite()
                    self.loadingIndicator.hidden = true
                    self.loadingIndicator.stopAnimating()
                }
            case .Failure(let error):
                print(error)
                self.loadingIndicator.hidden = true
                self.loadingIndicator.stopAnimating()
            }
        }
    }
    
    @IBAction func fieldChanged(sender: AnyObject) {
        let realm = try! Realm()
        let searchValue = searchTB.text!
        
        if(searchValue != ""){
        let suggestions = realm.objects(TypeAheadRecord).filter("City BEGINSWITH[c] %@", searchValue)
         var tempText = ""
            if(suggestions.count > 0){
            resultsCount.text = addZeros(suggestions[0].ZipCode)+" : \(suggestions[0].lat):\(suggestions[0].lng)"
            centerPoint = suggestions[0]
            performRadiusSearchAction()
            }
            else{
            resultsCount.text = "no suggestions"
            searchResults.text = "no results"
            }
            for city in suggestions{
                tempText+=city.City+", "+city.State+" "+city.ZipCode+"\n"
            }

        textOutput.text = tempText
        }
        else{
        textOutput.text = "[-type ahead suggestions listed here-]"
        resultsCount.text = ""
        searchResults.text = "[-results within 20 miles of search sorted by 'deadhead' miles-]"
        }
        
    }
    
    func performRadiusSearchAction(){
        let radius = 32186.9 // 20 miles
        let tempCoord = CLLocationCoordinate2D(latitude: centerPoint.lat, longitude: centerPoint.lng)
        let centerLocation = CLLocation(latitude: centerPoint.lat, longitude: centerPoint.lng)
    
        results = try! Realm().findNearby(TypeAheadRecord.self, origin: tempCoord, radius: radius, sortAscending: true)
        if(results!.count > 0){
            var tempText = "Results: \(results!.count) \n"
            for city in results!{
                let cityLocation = CLLocation(latitude: city.lat, longitude: city.lng)

                let tempDistance: CLLocationDistance = centerLocation.distanceFromLocation(cityLocation)
                tempText+="[\(Int(metersToMiles(tempDistance))):miles] "+city.City+", "+city.State+" "+city.ZipCode+"\n"
            }
            searchResults.text = tempText
        }
        else{
            searchResults.text = "no suggestions"
        }
    }
    func addZeros(zip:String) -> String{
        if(zip.characters.count == 1){
            return "0000"+zip
        }
        if(zip.characters.count == 2){
            return "000"+zip
        }
        if(zip.characters.count == 3){
            return "00"+zip
        }
        if(zip.characters.count == 4){
            return "0"+zip
        }
        return zip
    }
    func metersToMiles(meters:Double)->Double{
        let tempMiles = meters * 0.000621371192
        return tempMiles
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

